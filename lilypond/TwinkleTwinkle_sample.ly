#(set-default-paper-size "letter")

\version "2.18.0"

\layout { indent = 0.0\cm }

melodyOne = {
  g4 g d' d | e e d2 | c4 c b b | a a g2 | \break
  d'4 d c c | b b a2 | d4 d c c | b b a2 | \break
  g4 g d' d | e e d2 | c4 c b b | a a g2 |
}

melodyTab = {
  \transpose g d {
  g4 g d'\3 d'\3 | e'\3 e'\3 d'2\3 | c'4 c' b b | a a g2 |
  d'4\3 d'\3 c'\3 c'\3 | b b a2 | d'4\3 d'\3 c' c' | b b a2 |
  g4 g d'\3 d'\3 | e'\3 e'\3 d'2\3 | c'4\3 c'\3 b b | a a g2 |
  }
}

verseOne = \lyricmode {
  Twin -- kle, twin kle, lit -- tle, star,
  How I won -- der what you are.
  Up a -- bove the world so high,
  Like a dia -- mond in the sky.
  Twin -- kle, twin kle, lit -- tle star,
  How I won -- der what you are!
}

\score {
  <<
  \new TabStaff
    \with {
	       stringTunings = #`(
			     ,(ly:make-pitch  0 1 0)
               		     ,(ly:make-pitch -1 5 0)
	       		     ,(ly:make-pitch -1 1 0)
               )
	       %TabStaff.stringTunings = #'(14 11 7 2 19 )
               fretLabels = #'(
               "0"   "0+"  "1"  "1+" "2"  "3"  "3+"  "4"  "4+"  "5"  "6"  "6+"
               "7"   "7+"  "8"  "8+" "9"  "10" "10+" "11" "11+" "12" "13" "13+"
               "14" "14+" "15" "15+" "16" "17" "17+" "18" "18+" "19" "20" "20+"
               )
               tablatureFormat = #fret-letter-tablature-format
               fontSize = #2
    }
    \new TabVoice {
       \melodyTab
    }
  >>
}
