\header {
  title = "Yester Oak"
  composer = "Jakim Friant"
  instrument = "Guitar"
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\include "predefined-guitar-fretboards.ly"

\layout { indent = 0.0\cm }

verse = {
  \key c \major       
    \chordmode {
      c8 c c4 a8:m a:m a:m a:m | g2 d8:m d4.:m | \break
      c4 c a:m a8:m a8:m | g8 g g g d8:m d4.:m | \break
      c4 c a8:m a:m a:m a:m | g8 g g g d8:m d4.:m | \break
      c4 c a4:m a:m | g8 g g g d8:m d4.:m | \break
   }
}

verseAlt = {
  \key c \major       
    \chordmode {
      c4 c a4:m a:m | g2 d8:m d4.:m |
    }
}
verseOneWords = \lyricmode {
  Sit down and talk with me you said _ _ |
  Never bo -- thered by the way you look or what went-on |
  Se -- ven days is ne -- ver e -- nough for two _ _ |
  Time goes its way no mat -- ter what's been done |
}

bridge = {
  \key c \major \bar "||" \chordmode {
    \repeat volta 2 { 
      e8.:m7 e8.:m7 e8:m7 e8.:m7 e8.:m7 e8:m7 |
      d8.:m7 d8.:m7 d8:m7 c2 | 
    }
  }
}

chorus = {
  \key c \major \bar "||" \chordmode {
    c8. c8 c c16 c8. c8 c c16 |
    g8. g8 g g16 g8. g8 g g16 |
    a8.:m a8:m a:m a16:m a8.:m a8:m a:m a16:m | c8. c8 c c16 c8. c8 c c16 |
    c8. c8 c c16 c8. c8 c c16 | g8. g8 g g16 g8. g8 g g16 |
    a8.:m a8:m a:m a16:m a8.:m a8:m a:m a16:m | c8. c8 c c16 c8. c8 c c16 | \break
  }
}

chorusAlt = {
  \key c \major \bar "||" \chordmode {
    c2 g | a:m c | \break
    c2 g | a:m c | \break
  }
}

chorusWords = \lyricmode {
  all __ _ _ _ along __ _ _ _ |
  full__ _ of __ _ lies they say __ _ |
  in the lane where ghosts and sun -- beams | play __ _ _ _ _ _ _ _ |
  e __ _ _ _ -- ven though __ _ _ |
  it al -- ways hurts to try __ _ _ |
  you have to try, to know
}

\score {
  <<
  \new ChordNames {
    \verse
    \bridge
    \chorus
  }
  \new FretBoards {
    % set global properties of fret diagrams
    %\override FretBoards.FretBoard.size = #'1.2
      \verse
      \bridge
      \chorus
  }
  \new Staff {
    \new Voice = "one" {
      \verse
    }
    \bridge
    \new Voice = "two" {
      \chorus
    }
  }
  \new Lyrics \lyricsto "one" {
    \set stanza = #"1. "
      \verseOneWords
  }
  \new Lyrics \lyricsto "two" {
          \chorusWords
  }

  >>
}

\markup {
  Capo 3
}

\score {
  <<
  \new ChordNames {
    \transpose c ees \verseAlt
    \transpose c ees \bridge
    \transpose c ees \chorus
  }
  \new Staff {
    \transpose c ees \verseAlt
    \transpose c ees \bridge
    \transpose c ees \chorus
  }
  >>
}
