\header {
  title = "We Give You Thanks"
  poet = "Music and Words by Leslie Jordan, David Leonard and Jason Ingram"
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\include "predefined-guitar-fretboards.ly"

\layout { indent = 0.0\cm }

verse = {
  \key d \major       
    \chordmode {
  d2 d/+fis |
  g a:sus |
  d d/+fis |
  g a |
  b:m a/+cis |
  d g |
  a1
    }
}

chorus = {
  \key d \major \bar "||" \chordmode {
    g2 d |
    d/+fis b:m |
    g d |
    a1 |
    g2 d |
    d/+fis b:m |
    g d |
    b1:m |
    a2 g |
    b:m a:sus |
    g b:m |
    a1:sus
  }
}

\score {
  <<
  \new ChordNames {
    \verse
    \chorus
  }
  \new FretBoards {
    % set global properties of fret diagrams
    %\override FretBoards.FretBoard.size = #'1.2
      \verse
      \chorus
  }
  \new Staff {
    \verse
    \chorus
  }
  >>
}
