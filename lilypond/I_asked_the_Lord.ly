#(set-default-paper-size "letter")

\version "2.18.0"

\header {
  title = "I Asked the Lord"
  composer = "Laura Taylor"
  poet = "John Newton (alt. Laura Taylor)"
  instrument = \markup { Mountain Dulcimer (\smallCaps { dad }) }
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\layout { indent = 0.0\cm }

melodyOne = {
  <c e g>1 <f a c> <a c e> <f a c> <g b d> 
}

melodyTab = {
  <c'\3 e'\2 g'\1>1 <fis\3 a' cis'\1> <a c e> <f a c> <g b d> 
}

verseOne = \lyricmode {
  I asked the Lord that I might grow
  In faith and love and every grace
  Might more of His salvation know
  And seek more earnestly His face
}

verseTwo = \lyricmode {
  Twas He who taught me thus to pray
  And He I trust has answered prayer
  But it has been in such a way
  As almost drove me to despair
}


\score {
  <<
  \new Staff \relative c' {
      \new Voice = "one" {
        \key g \major
        \melodyOne
      }
  }
  \new Lyrics \lyricsto "one" {
    \verseOne
  }
  \new TabStaff
    \with {
	       stringTunings = #`(
			     ,(ly:make-pitch  0 1 0)
               		     ,(ly:make-pitch -1 5 0)
	       		     ,(ly:make-pitch -1 1 0)
               )
	       %TabStaff.stringTunings = #'(14 11 7 2 19 )
               fretLabels = #'(
               "0"   "0+"  "1"  "1+" "2"  "3"  "3+"  "4"  "4+"  "5"  "6"  "6+"
               "7"   "7+"  "8"  "8+" "9"  "10" "10+" "11" "11+" "12" "13" "13+"
               "14" "14+" "15" "15+" "16" "17" "17+" "18" "18+" "19" "20" "20+"
               )
               tablatureFormat = #fret-letter-tablature-format
               fontSize = #2
    }
    \new TabVoice {
       \melodyTab
    }
  >>
}
