\header {
  title = "Jesus Paid it All"
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\include "predefined-guitar-fretboards.ly"

\layout { indent = 0.0\cm }

verse = {
  \key b \major       
    \chordmode {
      b2:maj7/dis b:maj7 |
      fis b:maj7 |
      gis:m7 e:sus2 |
      b:maj7 fis |
      b1
    }
}

chorus = {
  \key b \major \bar "||" \chordmode {
    b2 gis:m7 |
    b fis |
    b e:sus2 |
    b fis |
    b1:maj7
  }
}

\score {
  <<
  \new ChordNames {
    \verse
    \chorus
  }
  \new FretBoards {
      \verse
      \chorus
  }
  \new Staff {
    \verse
    \chorus
  }
  >>
}
