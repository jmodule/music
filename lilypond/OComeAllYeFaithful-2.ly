#(set-default-paper-size "letter")

\version "2.18.0"
\header {
  title = "O Come, All Ye Faithful"
  composer = "John Francis Wade"
  poet = "Latin Hymn"
  instrument = \markup { Mountain Dulcimer (\smallCaps { dad }) }
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

\layout { indent = 0.0\cm }

verse = {
  \key d \major
  \partial 4
  d4 | d2 a4 d | e2 a, | fis'4 e fis g | fis2 e4 d | d~ d cis b | \break
  cis( d) e fis | cis2( b4.) a8 | a2. r4 | a'2 g4 fis | g~ g fis2 | 
  e4 fis d e | \break cis4.( b8) a4
}

verseTab = {
  \relative d
  \partial 4
  d,4\3 | d,2\3 a,4\3 d,\3 | e,2\3 a,\3 | fis4\3 e,\3 fis g |
  fis2 e4 d | d~ d cis' b | cis'( d) e fis |
  cis'2( b4.) a8 | a2. r4 | a2\3 g4 fis |
  g~ g fis2 | e4 fis d e | cis'4.( b8) a4
}


chorus = {
  \bar "||" d4 | d cis d e | d2 a4 fis' |
  fis e fis g | fis2 e4 fis | g fis e d |
  cis2 d4( g) | fis2( e4.) d8 | d2. r4 |
  \bar ":|."
}

chorusTab = {
  \bar "||" d4 | d cis' d e | d2 a4 fis |
  fis e fis g | fis2 e4 fis | g fis e d |
  cis'2 d4( g) | fis2( e4.) d8 | d2. r4 |
  \bar ":|."
}

verseOne = \lyricmode {
  O come, all ye faith -- ful, joy -- ful and tri -- um -- phant,
  O come ye, O come ye to Beth -- le -- hem!
  Come and be -- hold Him, born the King of an -- gels
}


verseThree = \lyricmode {
  O Sing choirs of an -- gels sing in ex -- ul -- ta -- tion
  O sing, __ all ye ci -- tizens of heav'n a -- bove;
  Glo -- ry to God, __ all glo -- ry in the highest:
}

verseFive = \lyricmode {
  Yea, Lord, we greet thee, born this ha -- ppy morn -- ing;
  \skip 1
  Je -- sus, to thee be all glo -- ry giv'n;
  Word of the Fa -- ther, now in flesh ap -- pear -- ing:
}

%Read more: Christmas Carols - O Come All Ye Faithful Lyrics | MetroLyrics

chorusAll = \lyricmode {
  O come, let us a -- dore Him, O come, let us a -- dore Him,
  O come, let us a -- dore him, Christ the Lord!
}

\score {
  <<
    \new Staff \relative d' {
      \new Voice = "one" {
        \tempo 4=88
        \transpose d e' {
          \verse
          \chorus
        }
      }
    }

  \new Lyrics \lyricsto "one" {
    \set stanza = #"1. "
    \verseOne
    \chorusAll
  }
  
  \new Lyrics \lyricsto "one" {
    \set stanza = #"3. "
    \verseThree
  }

  \new Lyrics \lyricsto "one" {
    \set stanza = #"5. "
    \skip 1
    \verseFive
  }


  \new TabStaff
    \with {
	       stringTunings = #`(
			     ,(ly:make-pitch  0 1 0)
               		     ,(ly:make-pitch -1 5 0)
	       		     ,(ly:make-pitch -1 1 0)
               )
	       %TabStaff.stringTunings = #'(14 11 7 2 19 )
               fretLabels = #'(
               "0"   "0+"  "1"  "1+" "2"  "3"  "3+"  "4"  "4+"  "5"  "6"  "6+"
               "7"   "7+"  "8"  "8+" "9"  "10" "10+" "11" "11+" "12" "13" "13+"
               "14" "14+" "15" "15+" "16" "17" "17+" "18" "18+" "19" "20" "20+"
               )
               tablatureFormat = #fret-letter-tablature-format
               fontSize = #2
    }
    \new TabVoice {
        \transpose d e' {
         \verseTab
         \chorusTab
        }
    }
  >>
}

\markup {
  \fill-line {
    \column {
      \hspace #0.1 % move the column off the left margin
      \line { \bold "Verse 2" }
      \line { God of God light of light }
      \line { Lo he not the virgin's womb; }
      \line { Very God begotten not created: }
      \vspace #1.0 % add a vertical space between verses      \column {
      \line { \bold "Verse 4" }
      \line { See how the shepards summoned to his cradel, }
      \line { leaving their flocks, draw nigh with lowly fear }
      \line { we too will thither hend our joyful footsteps; }
      \hspace #0.1 % give some space on the right margin
    }
  }
}

% for custom tab info
% see: http://lists.gnu.org/archive/html/lilypond-user/2011-07/msg00536.html
