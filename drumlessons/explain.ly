\version "2.12.3"

\header {
  title = "Explain"
  subtitle = "Jan 3, 2012"
  instrument = "Hi-Hat, Snare, and Kick Drum"
  texidoc = ""
}

drhv = \drummode { \repeat unfold 8 hihat8 }
drlv = \drummode { bassdrum4 snare8 bd8 r bd8 sn4 }

drhc = \drummode { \repeat unfold 16 hihat8 }
drlc = \drummode { bd8 bd snare4 bd4 sn4 bd8 bd sn8 bd8 r bd sn4 }

\score {
  \new DrumStaff <<
    \set Staff.instrumentName = #"verse"
    \new DrumVoice { \stemUp \drhv }
    \new DrumVoice { \stemDown \drlv }
  >>
  \layout { }
}

\score {
  \new DrumStaff <<
    \set Staff.instrumentName = #"chorus"
    \new DrumVoice { \stemUp \drhc }
    \new DrumVoice { \stemDown \drlc }
  >>
  \layout { }
}
