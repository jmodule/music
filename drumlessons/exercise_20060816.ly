#(ly:set-option 'old-relative)
\version "2.6.0"

\header {
    title = "Practice Exercises"
    subtitle = "Aug 16, 2006"
    instrument = "Tom-Tom, Snare, and Kick Drums"
    texidoc = "Practice Exercises."
}

nam = \lyricmode {
  Tom Snare Kick }
mus = \drummode {
  tomh sn bd }
\score {
  << \new DrumStaff \with {
       \remove Bar_engraver
       \remove Time_signature_engraver
       \override Stem #'transparent = ##t
       \override Stem #'Y-extent-callback = ##f
       minimumVerticalExtent = #'(-4.0 . 5.0)
     } \mus
     \context Lyrics \nam
  >>
  \layout {
    \context {
      \Score
      \override LyricText #'font-family = #'typewriter
      \override BarNumber #'transparent =##T
    }
  }
}

up = \drummode {
     tomh8^"1" tomh^"+" tomh^"2" tomh^"+" tomh^"3" tomh^"+" tomh^"4" tomh^"+"
     tomh^"1" tomh^"+" tomh^"2" tomh^"+" tomh^"3" tomh^"+" tomh^"4" tomh^"+"
     tomh^"1" tomh^"+" tomh^"2" tomh^"+" tomh^"3" tomh^"+" tomh^"4" tomh^"+"
     tomh^"1" tomh^"+" tomh^"2" tomh^"+" tomh^"3" tomh^"+" tomh^"4" tomh^"+"
}

down = \drummode {
%    snare4^"1" snare^"2" snare^"3" snare^"4"
%    snare^"1" snare^"2" snare^"3" snare^"4"
    bd4 sn4 bd sn
    bd sn bd sn
    bd sn bd sn
    bd sn bd sn
}

\score {
    \new DrumStaff <<
	\new DrumVoice { \voiceOne \up }
	\new DrumVoice { \voiceTwo \down }
    >>
}
