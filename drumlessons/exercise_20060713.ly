#(ly:set-option 'old-relative)
\version "2.2.0"

\header {
    title = "Practice Exercises"
    subtitle = "July 12, 2006"
    instrument = "Snare and Tom-tom"
    texidoc = "Practice Exercises."
}

timepractice = \notes \relative c{
    \clef "bass"
    d4 d d d d d d d d d d d d d d d
}

stickcontrol = \notes {
    \clef "bass"
    d4 d d r4 d d d r d d d r d d d r 
}

stickcontrol2 = \notes {
    \clef "bass"
    d4 d a, a, d d a, a, d d a, a, d d a, a,
}

timetext = \lyrics {
    one two three four one two three four one two three four one two three four
}

sticktext = \lyrics {
    one two three one two three one two three one two three
}

sticktext2 = \lyrics {
    snare snare tom tom
}

\score{
    <<
	\context Voice = one {
	    \set Staff.autoBeaming = ##f
	    \timepractice
	}
	\lyricsto "one" \new Lyrics \timetext
    >>
    \paper{}
}

\score {
    <<
	\context Voice = two {
	    \set Staff.autoBeaming = ##f
	    \stickcontrol
	}
	\lyricsto "two" \new Lyrics \sticktext
    >>
    \paper{}
}

\score {
    <<
	\context Voice = three {
	    \set Staff.autoBeaming = ##f
	    \stickcontrol2
	}
	\lyricsto "three" \new Lyrics \sticktext2
    >>
    \paper{}
}
