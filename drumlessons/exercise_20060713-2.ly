#(ly:set-option 'old-relative)
\version "2.6.0"

% up = \drummode { crashcymbal4 hihat8 halfopenhihat hh hh hh openhihat }
% down = \drummode { bassdrum4 snare8 bd r bd sn4 }

\header {
    title = "Practice Exercises"
    subtitle = "July 12, 2006"
    instrument = "Snare and Tom-tom"
    texidoc = "Practice Exercises."
}

timepractice = \drummode {
    snare4^"1" snare^"2" snare^"3" snare^"4"
    snare^"1" snare^"2" snare^"3" snare^"4"
    snare^"1" snare^"2" snare^"3" snare^"4"
    snare^"1" snare^"2" snare^"3" snare^"4"
}

stickcontrol = \drummode {
    snare4^"1" snare^"2" snare^"3" r4^"pause"
    snare4^"1" snare^"2" snare^"3" r4^"pause"
    snare4^"1" snare^"2" snare^"3" r4^"pause"
    snare4^"1" snare^"2" snare^"3" r4^"pause"
}

stickcontroltwo = \drummode {
    snare4_"snare"^"1" snare^"2" tomfh_"tom-tom"^"3" tomfh^"4"
    snare^"1" snare^"2" tomfh^"3" tomfh^"4"
    snare^"1" snare^"2" tomfh^"3" tomfh^"4"
    snare^"1" snare^"2" tomfh^"3" tomfh^"4"
}

\score {
    \new DrumStaff <<
        \set Staff.instrument = \markup { \center-align { Counting } }
	\context DrumVoice = "one" {
	    \set Staff.autoBeaming = ##f
	    \timepractice
	}
%        \addlyrics { count slowly and evenly }
    >>
    \layout{}
}

\score {
    \new DrumStaff<<
        \set Staff.instrument = \markup { \center-align { Stick Control } }
	\context DrumVoice = "two" {
	    \set Staff.autoBeaming = ##f
	    \stickcontrol
	}
    >>
    \layout{}
}

\score {
    \new DrumStaff <<
        \set Staff.instrument = \markup { \center-align { Alternate Drums } }
	\context DrumVoice = "three" {
	    \set Staff.autoBeaming = ##f
	    \stickcontroltwo
	}
    >>
    \layout{}
}
