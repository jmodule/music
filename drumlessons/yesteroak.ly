\version "2.12.3"

\header {
  title = "Yester Oak"
  subtitle = "Jan 3, 2012"
  instrument = "Hi-Hat, Snare, and Kick Drum"
  texidoc = ""
}

drup = \drummode {
  hhc8 hho8 hhc8 hh hh hh hh hh
  hhc8 hho8 hhc8 hh hh hh hh hh
}

drdown = \drummode {
  bd4 sn8 bd16 r bd8 bd8 sn4
  bd4 sn8 bd16 r bd8 bd8 sn4
}

\score {
  \new DrumStaff <<
    \set Staff.instrumentName = #"verse"
    \new DrumVoice { \stemUp \drup }
    \new DrumVoice { \stemDown \drdown }
  >>
  \layout { }
}

drupbr = \drummode { \repeat unfold 16 hihat8 }
drdnbr = \drummode {
  bd8 r16 bd16 sn8 bd8 bd8 r16 bd16 sn8 bd8
  bd8 r16 bd16 sn8 bd8 bd8 r16 bd16 sn4
}

\score {
  \new DrumStaff <<
    \set Staff.instrumentName = #"bridge"
    \new DrumVoice { \stemUp \drupbr }
    \new DrumVoice { \stemDown \drdnbr }
  >>
  \layout { }
}

drupch = \drummode { hh4^"double time" \repeat unfold 7 hh4 }
drdnch = \drummode { 
  bd4 sn4 bd8 bd8 sn8
  bd8 r bd8 sn4 bd8 bd8 sn4
}

\score {
  \new DrumStaff <<
    \set Staff.instrumentName = #"chorus"
    \new DrumVoice { \stemUp \drupch }
    \new DrumVoice { \stemDown \drdnch }
  >>
  \layout { }
}
